---
date: 2017-06-07
title: Community
---

---
### Social networks
Follow OpenCert and the AMASS Open Platform on:

* [Twitter](https://twitter.com/@AMASSproject)
* [LinkedIn Group](https://www.linkedin.com/groups/3807241/)

View more videos on the [youtube channel](https://www.youtube.com/channel/UCw_D0l5sDgysEphi6tzzDyw)

### Get in touch with the team

Learn about OpenCert, join the community, connect with other CHESS users, get support and service:

* [Forum](https://www.eclipse.org/forums/): Consult the OpenCert forum and ask questions about the tool. This also the forum about CHESS and other Polarsys projects.
* [Polarsys Wiki](https://wiki.eclipse.org/Polarsys): Wiki providing additional resources about OpenCert and Polarsys.
* [Report a bug](https://bugs.eclipse.org/bugs/describecomponents.cgi?product=Opencert): Bugzilla for bug reporting and enhancement requests.
* [Mailing-list](https://accounts.eclipse.org/mailing-list/opencert-dev): Subscribe to the developers mailing-list to be warned about technical announcements.
* [PMI page](https://projects.eclipse.org/projects/polarsys.opencert): information about the project development.

### Source Repositories

You can use the code from these repositories to experiment, test, build, create patches, issue pull requests, etc. This project is hosted on gitlab.eclipse.org. Please use GitLab issues or [OpenCert bugzilla product](https://bugs.eclipse.org/bugs/describecomponents.cgi?product=Opencert).

Project GitLab is hosted [here](https://gitlab.eclipse.org/eclipse/opencert).

Clone:  [https://gitlab.eclipse.org/eclipse/opencert/opencert.git](https://gitlab.eclipse.org/eclipse/opencert/opencert.git)

Browse: [https://gitlab.eclipse.org/eclipse/opencert/opencert](https://gitlab.eclipse.org/eclipse/opencert/opencert)
