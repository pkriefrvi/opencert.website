---
date: 2018-03-22
title: Online training
---

This page references the latest training material available for the OpenCert platform.

<!--more-->

## Training - Winter series 2018
### Training on the Prototype P1: WP3 Session #1 (26 Jan 2018)
* Workflow overview
* System Component Specification: Requirement specification
* Requirement Formalization: Formalize requirements with formal properties, Contract editor with content assist
* Requirement Early Validation: Metrics
* Functional Refinement: Architectural refinement, Contract refinement, Contract-based views
{{< youtube EysXR-JSilM >}}

### Training on the Prototype P1: WP3 Session #2 (31 Jan 2018)
* Components nominal and faulty behaviour definition
* Functional early verification: Integration of CHESS and V&V tools, Contract refinement analysis, Contract validation, Contract-based verification of refinement
* Safety analysis: Fault tree generation, Contract-based safety analysis
* Safety Case: Generate product-based assurance arguments from CHESS model, Link architecture-related entities, Document generation
* Upcoming features: Savona, Simulation-based Fault Injection, Requirement Early Validation
{{< youtube lK8bfr_uz6c >}}

### Training on the Prototype P1: WP4 Session #1 (25 Jan 2018)
* Dependability assurance case modelling
* Contract-based multi-concern assurance
* System-dependability co-analysis: via Papyrus, via Safety Architect and via Concerto
{{< youtube oEopBXgxxkc >}}

### Training on the Prototype P1: WP6 Session (24 Jan 2018)
* Introduction to filtering at Assurance Project generation by Criticality/Applicability level
* Reuse Assistant
* Compliance Map Report
* Management of Families-Lines: Variability Management support at process level and at component level, cross-concern variability management
* Semi-automatic generation of Arguments: Product arguments and process arguments
{{< youtube XHt2SkzL7XU >}}
