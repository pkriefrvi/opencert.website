---
date: 2018-12-05
title: Documentation
---

Access to the documentation of the current OpenCert package produced by the AMASS partners - code name Prototype P2.

<!--more-->

* [User Manual](AMASS-Platform-P2-User-Manual_23112018.pdf)

* [Developer Manual](AMASS-Platform-P2-Developers-Guide_23112018.pdf)

---
### Related documentation
* AMASS Project Deliverables: http://www.amass-ecsel.eu/content/deliverables
* CHESS User Guide: https://www.polarsys.org/chess/publis/CHESSToolset_UserGuide.pdf
* EPF User Manual: https://eclipse.org/epf/general/EPF_Installation_Tutorial_User_Manual.pdf
* Structured Assurance Case Metamodel™ (SACM™) Standard: http://www.omg.org/spec/SACM/
