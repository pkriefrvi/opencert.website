---
date: 2017-06-07
title: About
---

---

## Components of the AMASS Tool Platform

OpenCert is not a monolithic platform. The OpenCert Tool Platform is composed of various contributions that cover the different features:

* <img src="../images/logo.png" class="img-responsive" alt="OpenCert logo" width="100"> [OpenCert Core](https://www.polarsys.org/projects/polarsys.opencert) toolset supports evidence management, assurance case specification and compliance management functionalities.
* <img src="../images/chess-logo.png" class="img-responsive" alt="Chess logo" width="100"> The [Chess toolset](https://www.polarsys.org/chess/) adds support for Architecture-Driven Assurance. The CHESS toolset leverages another important PolarSys project, the Papyrus platform for UML design and profiles.
* [EPF Composer](http://www.eclipse.org/epf/) supports the process specification and process compliance.
* [BVR Tool](http://www.amass-ecsel.eu/content/bvr-tool-amass) provides support in Eclipse for the Base Variability Revolution Language
* [Eclipse Capra](http://projects.eclipse.org/projects/modeling.capra) supports traceability between different 	artefacts in the platform.
* [Eclipse CDO](http://www.eclipse.org/cdo/) is used to store all the models.

## AMASS Project partners

The main contributions come from the [AMASS project](http://www.amass-ecsel.eu). The reuse of existing open source projects shows the pragmatism of the approach, and the application of a best practices of software engineering and open source communities: “join and contribute to existing communities” instead of “reinventing the wheel”. As such, the OpenCert open source platform is a good example of the advantages of open source software that can be combined and extended more easily as all APIs and formats are open.

<img src="http://www.amass-ecsel.eu/sites/amass.drupal.pulsartecnalia.com/files/pictures/PartnersMap_nov2016.PNG" class="img-responsive" alt="CAMASS partners">

