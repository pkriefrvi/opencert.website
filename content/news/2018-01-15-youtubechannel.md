---
date: 2018-01-15
title: New YouTube channel
link: https://www.youtube.com/channel/UCw_D0l5sDgysEphi6tzzDyw
---

Subscribe to the OpenCert Youtube

<!--more-->
<a href="https://www.youtube.com/channel/UCw_D0l5sDgysEphi6tzzDyw"><img src="../../images/youtube.png"  alt="Youtube"></a>

The AMASS partners regularly publish new content on the [OpenCert](https://www.youtube.com/channel/UCw_D0l5sDgysEphi6tzzDyw) Youtube channel.

Subscribe to the channel to get notified of new videos!

_Article by **Gaël Blondelle**, Eclipse Foundation_

