---
date: 2018-12-05
title: Download the new version of the AMASS open platform!
---

AMASS partners just published their latest version of the AMASS open tools platform.

<!--more-->
Go to the [download](../../downloads) page and give it a try!

_Article by **Gaël Blondelle**, Eclipse Foundation_

