---
date: 2017-06-07
title: Building the website
---

In 2017 spring, we are starting to build a website for the Eclipse OpenCERT project.

<!--more-->

You will find all the information about the project, including tutorials and documentation.

The Eclipse Foundation is provisioning resources like Forum, Wiki, Bugzilla and Mailing-list, so please stay tuned.

_Article by **Antoine THOMAS**, Eclipse Foundation_
