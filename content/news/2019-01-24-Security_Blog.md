---
date: 2019-01-24
title: Considerations about open source and security
---

Security and openness are two orthogonal issues and the AMASS Open Tool Platform is certainly not a liability for the development of CPS

<!--more-->
Read the [blog post](/opencert/resources/security/) .

_Blog Post by **Maria Teresa Delgado** and **Gaël Blondelle**, Eclipse Foundation_

