---
date: 2018-03-22
title: New content on the OpenCert website
---

To accompany their new prototype, the AMASS partners updated the web site with new content. 

<!--more-->
New documentation has been put together in the [resources](/opencert/resources/) section.

_Article by **Gaël Blondelle**, Eclipse Foundation_

