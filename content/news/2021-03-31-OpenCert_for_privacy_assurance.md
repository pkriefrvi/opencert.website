---
date: 2021-03-31
title: OpenCert for privacy assurance, thanks to the PDP4E project
---

Assurance was one key aspect of the [PDP4E project](http://pdp4e-project.eu) (Privacy and Data Protection for Engineering) H2020 European Project No 787034.

In this project, it was demonstrated the feasibility to use Eclipse OpenCert also for privacy assurance through the modelling of privacy reference frameworks, mapping models and privacy assurance patterns.

The public PDP4E knowledge base for privacy assurance is currently at the “release/2.0” branch in the “examples/privacy” folder. This branch also contains the initial contribution for OpenCert 2.0 where several improvements and new functionalities were developed thanks to the PDP4E project and its application in the privacy-related use cases.

_Blog Post by **Jabier Martinez Perdiguero**, Tecnalia_


