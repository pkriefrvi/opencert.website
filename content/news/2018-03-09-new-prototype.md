---
date: 2018-03-09
title: New integrated prototype built by the AMASS project team
---

The AMASS project partners invested tons of energy to build the new prototype of the AMASS Open Platform. 

<!--more-->
Read more information at on [the project website](https://www.amass-ecsel.eu/content/amass-prototype-p1)

_Article by **Gaël Blondelle**, Eclipse Foundation_

