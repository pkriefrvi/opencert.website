---
date: 2018-06-25
title: Back from EclipseCon France 2018
---

AMASS was present on stage during the plenary session. Atif explains the process to migrate EPF to the latest version of the Eclipse platform.

<!--more-->
![Atif at EclipseCon France](../../images/201806AtifEclipseConFrance.jpg "Atif at EclipseCon France")

At Eclipse France 2018, Muhammad Atif Javed from MDH, explained the whole attendance
the process of migrating Eclipse Process Framework composer to the latest version of Eclipse.

During the second half of 2017, Mälardalen University (MDH), with the help of
the Eclipse Foundation, started an important contribution to the Eclipse Process
Framework project and more specifically to EPF Composer. The project was under
maintenance since 2013, with no new official release since then, and still
running on an old version of the Eclipse platform.

Due to this too old version of the Eclipse platform, it was not possible to
integrate EPF Composer in the AMASS open platform prototype.

MDH stepped up to fix this, and with the help of the Eclipse Foundation,
the AMASS partners started to collaborate with the IBM team which was
maintaining EPF Composer.

After initial contacts, MDH developed a patch to port EPF to Eclipse Neon.
This patch was then tested by the EPF development team before integration.
Later, as MDH designated developer, Muhammad Atif Javed, had proved his ability
to contribute to the project, he was elected as a new committer of the EPF
project in conformance with the principles of meritocracy promoted by the
Eclipse Development Process.

Then, the EPF project team collaborated to create a new release,
which is the one integrated in the AMASS Open Platform.

_Article by **Gaël Blondelle**, Eclipse Foundation_


